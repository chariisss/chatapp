// 1. Sebutkan 5 props yg berada pada stack.screen?
//    title,headerShown,headerLeft, headerTitle, route

// 2. Apa itu pormise?
//    Promise adalah janji. Promise adalah sebuah objek yang mepresentasikan keberhasilan / kegagalan sebuah event yang asynchronous yang akan terjadi
//    terdapat 2 kondisi, yaitu
// 	states(fullfilled/rejected/pending)
// 	callback(resolve/reject/finally)
// 	aksi nya kalau terpenuhi(then) kalau tidak (catch)
//    contoh:
// const dataDiri = new Promise(resolve => {
//   setTimeout(() => {
//     resolve([
//       {
//         nama: 'Charis',
//         alamat: 'Tangerang',
//         umur: '23',
//       },
//     ]);
//   });
// });

// const cuaca = new Promise(resolve => {
//   setTimeout(() => {
//     resolve([
//       {
//         kota: 'Tangerang',
//         suhu: 30,
//         kondisi: 'Cerah',
//       },
//     ]);
//   });
// });

// Promise.all([dataDiri, cuaca]).then(response => {
//   const [dataDiri, cuaca] = response;
//   console.log(dataDiri);
//   console.log(cuaca);
// });

// 3. Kegunaan await pada awal componentDidMount?
// Async/Await adalah salah satu cara untuk mengatasai masalah asynchronous pada Javascript selain menggunakan callback dan promise.
// Untuk menggunakan Async/Await, kembalian dari suatu fungsi harus merupakan suatu Promise. Async/Await tidak dapat berdiri tanpa adanya Promise.
// Tidak seperti Promise, dengan Async/Await maka suatu baris kode dapat tersusun rapi mirip dengan kode yang sifatnya synchronous.
// Pada implementasi Async/Await, kita menggunakan kata kunci async sebelum fungsi. Await sendiri hanya bisa digunakan pada fungsi yang menggunakan async.
// Fungsi yang menggunakan async, sebenarnya mempunyai kembalian berupa promise. Tetapi, Async/Await tidak dapat digunakan di top level kode kita, karena harus dibungkus oleh suatu fungsi.
// Setiap baris yang menggunakan await, akan ditunggu sampai Asynchronous Promise tersebut di resolve.

// 4.flow login
//  User memasukan email dan password
//  front end akan memvalidasi sederhana data yang user input
//  from dari front end akan dikirim ke back end untuk dilakukan validasi ke database
// ketika valid, back end akan mengirimkan token ke front end
// token akan disimpan pada sesion storage pada front end

// 5. async flow login
// fungsi login menunggu data dari backend setelah beberapa waktu
