import React, {Component} from 'react';
import {Alert} from 'react-native';
import messaging from '@react-native-firebase/messaging';
import Navigation from './src/navigation/Navigation';
import AsyncStorage from '@react-native-async-storage/async-storage';

export class App extends Component {
  async componentDidMount() {
    // const store = await [];

    // //get last async storea data
    // const lastMessage = await AsyncStorage.getItem('inbox');
    // store.push(JSON.parse(lastMessage));
    // await messaging().onMessage(async remoteMessage => {
    //   const message = await {
    //     body: remoteMessage.notification.body,
    //     title: remoteMessage.notification.title,
    //   };
    //   Alert.alert('A new FCM message arrived!', message[0]);
    //   const dataMessage = await JSON.stringify(message);
    //   await store.push(message);
    //   AsyncStorage.setItem('inbox', JSON.stringify(store));
    // });

    // await console.log(`store :`, store);

    messaging().onMessage(async message => {
      const title = message.notification.title;
      const body = message.notification.body;
      Alert.alert(title, body);
      AsyncStorage.getItem('inbox')
        .then(resultInbox => {
          const inbox = JSON.parse(resultInbox);
          AsyncStorage.setItem(
            'inbox',
            JSON.stringify([{title: title, body: body}, ...inbox]),
          );
        })
        .catch(err => {
          AsyncStorage.setItem(
            'inbox',
            JSON.stringify([{title: title, body: body}]),
          );
        });
    });

    messaging().setBackgroundMessageHandler(async message => {
      const title = message.notification.title;
      const body = message.notification.body;
      Alert.alert(title, body);
      AsyncStorage.getItem('inbox')
        .then(resultInbox => {
          const inbox = JSON.parse(resultInbox);
          AsyncStorage.setItem(
            'inbox',
            JSON.stringify([{title: title, body: body}, ...inbox]),
          );
        })
        .catch(err => {
          AsyncStorage.setItem(
            'inbox',
            JSON.stringify([{title: title, body: body}]),
          );
        });
      // console.log('Message handled in the background!', remoteMessage);
    });
  }
  render() {
    return (
      <>
        <Navigation />
      </>
    );
  }
}

export default App;
