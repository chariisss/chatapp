import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  Pressable,
  ScrollView,
  TextInput,
} from 'react-native';
import Colors, {
  orange,
  orange2,
  gray,
  gray2,
  dark,
  dark2,
  dark3,
  dark4,
  white,
  white2,
  green,
} from '../../utils/Colors';
import ButtonC from '../../component/ButtonC';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {styles} from './style';
import {heightPercentageToDP, widthPercentageToDP} from '../../utils/Query';

export class CodeVerif extends Component {
  render() {
    return (
      <View style={{flex: 1, backgroundColor: dark}}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text
            style={{
              fontSize: heightPercentageToDP(3),
              color: orange,
              marginBottom: heightPercentageToDP(3),
            }}>
            We have been send default password
          </Text>
          <Text
            style={{
              fontSize: heightPercentageToDP(3),
              color: orange,
              marginBottom: heightPercentageToDP(3),
            }}>
            to your email, please check it
          </Text>
          <ButtonC
            text="Create New Password"
            onPress={() => {
              this.props.navigation.navigate('New Password');
            }}
          />
        </View>
      </View>
    );
  }
}

export default CodeVerif;
