import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  Pressable,
  ScrollView,
  TextInput,
} from 'react-native';
import Colors, {
  orange,
  orange2,
  gray,
  gray2,
  dark,
  dark2,
  dark3,
  dark4,
  white,
  white2,
  green,
} from '../../utils/Colors';
import ButtonC from '../../component/ButtonC';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {styles} from './style';
import {heightPercentageToDP, widthPercentageToDP} from '../../utils/Query';

export class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      viewPassword: true,
    };
  }
  render() {
    return (
      <ScrollView style={styles.global}>
        <View style={{alignItems: 'center', justifyContent: 'center'}}>
          <Text style={styles.textJudul}>Chat App</Text>
        </View>
        <View style={styles.container}>
          <Text style={styles.textSignUp}>Sign Up</Text>
          <Text style={styles.textSubSignUp}>
            Enter details to create a new account
          </Text>
          <View style={[styles.button, {backgroundColor: dark2}]}>
            <Ionicons name="person" size={25} color={white} />
            <TextInput
              placeholder="Full Name"
              placeholderTextColor={white}
              style={styles.textInput}
            />
          </View>
          <View style={styles.button2}>
            <Ionicons name="mail" size={25} color={gray} />
            <TextInput
              placeholder="Email"
              placeholderTextColor={gray}
              style={styles.textInput}
            />
          </View>
          <View style={styles.button2}>
            <Ionicons name="lock-closed" size={25} color={gray} />
            <TextInput
              placeholder="Password"
              placeholderTextColor={gray}
              style={styles.textInput}
              secureTextEntry={this.state.viewPassword}
            />
            <Ionicons
              name={this.state.viewPassword ? 'eye-off-outline' : 'eye-outline'}
              size={25}
              color={gray}
              style={{marginLeft: 'auto', marginRight: widthPercentageToDP(5)}}
              onPress={() => {
                this.setState({viewPassword: !this.state.viewPassword});
              }}
            />
          </View>
          <View style={styles.button2}>
            <Ionicons name="lock-closed" size={25} color={gray} />
            <TextInput
              placeholder="Confirm Password"
              placeholderTextColor={gray}
              style={styles.textInput}
              secureTextEntry={this.state.viewPassword}
            />
            <Ionicons
              name={this.state.viewPassword ? 'eye-off-outline' : 'eye-outline'}
              size={25}
              color={gray}
              style={{marginLeft: 'auto', marginRight: widthPercentageToDP(5)}}
              onPress={() => {
                this.setState({viewPassword: !this.state.viewPassword});
              }}
            />
          </View>
          <ButtonC
            text="Sign Up"
            onPress={() => {
              this.props.navigation.navigate('Home');
            }}
          />
          <Text style={styles.forgotPassword}>
            Forgot Password?{' '}
            <Text
              style={{color: orange}}
              onPress={() => {
                this.props.navigation.navigate('Reset Password');
              }}>
              Reset Password
            </Text>
          </Text>
          <Text style={{color: white2}}>
            Already have an account?{' '}
            <Text
              style={{color: orange}}
              onPress={() => {
                this.props.navigation.navigate('Login');
              }}>
              Sign In
            </Text>
          </Text>
        </View>
      </ScrollView>
    );
  }
}

export default SignUp;
