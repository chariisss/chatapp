import {StyleSheet} from 'react-native';
import Colors, {
  orange,
  orange2,
  gray,
  gray2,
  gray3,
  dark,
  dark2,
  dark3,
  dark4,
  white,
  white2,
  green,
  chocolate,
} from '../../utils/Colors';
import {heightPercentageToDP, widthPercentageToDP} from '../../utils/Query';

const styles = StyleSheet.create({
  global: {
    flex: 1,
    backgroundColor: dark,
  },
  topBar: {
    flexDirection: 'row',
    alignItems: 'center',
    height: heightPercentageToDP(8),
    backgroundColor: gray2,
    paddingHorizontal: widthPercentageToDP(5),
  },
  foto: {
    width: widthPercentageToDP(11),
    height: widthPercentageToDP(11),
    marginHorizontal: widthPercentageToDP(3),
    borderRadius: 50,
    overflow: 'hidden',
    borderWidth: 2,
    borderColor: chocolate,
  },
  img: {
    width: '100%',
    height: '100%',
  },
  name: {
    fontSize: widthPercentageToDP(6),
    fontWeight: 'bold',
    color: orange,
  },
  isiChat: {
    width: widthPercentageToDP(100),
    height: heightPercentageToDP(82),
    backgroundColor: dark,
  },
  botBar: {
    height: heightPercentageToDP(8),
    width: widthPercentageToDP(100),
    backgroundColor: dark,
    paddingHorizontal: widthPercentageToDP(5),
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputChat: {
    height: heightPercentageToDP(6),
    width: widthPercentageToDP(79),
    marginLeft: widthPercentageToDP(1),
    backgroundColor: gray2,
    borderRadius: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  textInputStyle: {
    color: white,
    marginLeft: widthPercentageToDP(5),
    width: '100%',
  },
  sendIcon: {
    marginLeft: 'auto',
    marginRight: widthPercentageToDP(5),
    color: orange,
  },
});

export {styles};
