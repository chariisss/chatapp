import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  Pressable,
  ScrollView,
  TextInput,
} from 'react-native';
import Colors, {
  orange,
  orange2,
  gray,
  dark,
  dark2,
  white,
  white2,
  green,
  dark3,
  dark4,
  chocolate,
  gray2,
} from '../../utils/Colors';
import ButtonC from '../../component/ButtonC';
import ChatItemC from '../../component/ChatItemC';
import Ionicons from 'react-native-vector-icons/Ionicons';
import firestore from '@react-native-firebase/firestore';
import {heightPercentageToDP, widthPercentageToDP} from '../../utils/Query';
import {styles} from './style';

export class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.route.params,
    };
  }

  render() {
    console.log(this.state.data);
    const {foto, nama} = this.state.data;

    return (
      <View style={styles.global}>
        <View style={styles.topBar}>
          <Ionicons
            name="chevron-back-circle"
            size={33}
            style={{color: white}}
            onPress={() => {
              this.props.navigation.goBack();
            }}
          />
          <View style={styles.foto}>
            <Image source={{uri: foto}} style={styles.img} />
          </View>
          <Text style={styles.name}>{nama}</Text>
        </View>
        <ScrollView style={styles.isiChat}>
          <View
            style={{
              maxWidth: widthPercentageToDP(80),
              backgroundColor: '#ffff',
              borderRadius: 5,
              minHeight: 20,
              padding: 5,
              margin: 5,
            }}>
            <Text>aaaaaaaaaaaaaaaaaaaa</Text>
          </View>
        </ScrollView>
        <View style={styles.botBar}>
          <Ionicons name="add-circle-sharp" size={35} style={{color: white}} />
          <View style={styles.inputChat}>
            <TextInput
              placeholder="Type a message"
              placeholderTextColor={white}
              style={styles.textInputStyle}
            />
            <Ionicons name="send" size={20} style={styles.sendIcon} />
          </View>
        </View>
      </View>
    );
  }
}

export default Chat;
