import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  Pressable,
  ScrollView,
  TextInput,
} from 'react-native';
import Colors, {
  orange,
  orange2,
  gray,
  gray2,
  dark,
  dark2,
  dark3,
  dark4,
  white,
  white2,
  green,
} from '../../utils/Colors';
import ButtonC from '../../component/ButtonC';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {styles} from './style';
import {heightPercentageToDP, widthPercentageToDP} from '../../utils/Query';

export class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      viewPassword: true,
    };
  }
  render() {
    return (
      <ScrollView style={styles.global}>
        <Ionicons
          name="chevron-back-circle"
          size={33}
          style={{
            marginBottom: heightPercentageToDP(3),
            marginTop: heightPercentageToDP(2),
            marginLeft: widthPercentageToDP(5),
            color: white,
          }}
          onPress={() => {
            this.props.navigation.goBack('');
          }}
        />
        <View style={{alignItems: 'center', justifyContent: 'center'}}>
          <Text style={styles.textJudul}>Chat App</Text>
        </View>
        <View style={styles.container}>
          <Text style={styles.textSignUp}>Forget Password</Text>
          <Text style={styles.textSubSignUp}>
            Enter your email to recover password
          </Text>
          <View
            style={[
              styles.button,
              {
                backgroundColor: dark4,
                marginVertical: heightPercentageToDP(13),
              },
            ]}>
            <Ionicons name="mail" size={25} color={gray} />
            <TextInput
              placeholder="Enter your email"
              placeholderTextColor={gray}
              style={styles.textInput}
            />
          </View>
          <ButtonC
            text="Confirm"
            onPress={() => {
              this.props.navigation.replace('Code Verif');
            }}
          />
        </View>
      </ScrollView>
    );
  }
}

export default ResetPassword;
