import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import Colors, {
  orange,
  orange2,
  gray,
  dark,
  dark2,
  white,
  white2,
  green,
} from '../../utils/Colors';
import {heightPercentageToDP, widthPercentageToDP} from '../../utils/Query';

const styles = StyleSheet.create({
  global: {flex: 1, backgroundColor: dark},
  container: {alignItems: 'center', justifyContent: 'center', flex: 1},
  text: {
    color: orange,
    fontSize: heightPercentageToDP(4.2),
  },
  button: {
    height: heightPercentageToDP(7),
    width: widthPercentageToDP(70),
    backgroundColor: orange2,
    position: 'absolute',
    bottom: 0,
    marginBottom: heightPercentageToDP(10),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
});

export {styles};
