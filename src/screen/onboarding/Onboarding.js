import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  Pressable,
  ScrollView,
} from 'react-native';
import ButtonC from '../../component/ButtonC';
import Colors, {
  orange,
  orange2,
  gray,
  dark,
  dark2,
  white,
  white2,
  green,
} from '../../utils/Colors';
import {heightPercentageToDP, widthPercentageToDP} from '../../utils/Query';
import {styles} from './style';

export class Onboarding extends Component {
  constructor() {
    super();
  }
  render() {
    return (
      <View style={styles.global}>
        <View style={styles.container}>
          <Image
            source={require('../../img/onboarding.png')}
            style={{
              height: heightPercentageToDP(30),
              width: widthPercentageToDP(90),
            }}
          />
          <Text
            style={[
              styles.text,
              {
                marginTop: heightPercentageToDP(3),
              },
            ]}>
            Stay Connected
          </Text>
          <Text
            style={[
              styles.text,
              {
                marginTop: heightPercentageToDP(1),
                marginBottom: heightPercentageToDP(10),
              },
            ]}>
            With Your Friends!
          </Text>
          <ButtonC
            text="Get Started"
            onPress={() => {
              this.props.navigation.navigate('Login');
            }}
          />
        </View>
      </View>
    );
  }
}

export default Onboarding;
