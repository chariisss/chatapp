import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import Colors, {
  orange,
  orange2,
  gray,
  dark,
  dark2,
  dark3,
  dark4,
  white,
  white2,
  green,
} from '../../utils/Colors';
import {heightPercentageToDP, widthPercentageToDP} from '../../utils/Query';
const styles = StyleSheet.create({
  global: {
    flex: 1,
    backgroundColor: dark,
    paddingTop: heightPercentageToDP(10),
  },
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  textJudul: {
    fontSize: heightPercentageToDP(7),
    color: orange,
    fontWeight: 'bold',
  },
  textLogin: {
    fontSize: heightPercentageToDP(4),
    color: orange,
    fontWeight: 'bold',
    marginTop: heightPercentageToDP(6),
  },
  textSubLogin: {
    fontSize: heightPercentageToDP(2.2),
    color: orange,
    marginTop: heightPercentageToDP(1),
    marginBottom: heightPercentageToDP(6),
  },
  textInput: {
    color: white2,
    marginLeft: widthPercentageToDP(3),
    width: '100%',
  },
  button: {
    height: heightPercentageToDP(7),
    width: widthPercentageToDP(80),
    borderColor: orange,
    borderWidth: 1,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: widthPercentageToDP(5),
  },
  forgotPassword: {
    marginTop: heightPercentageToDP(5),
    marginBottom: heightPercentageToDP(2),
    color: white2,
  },
});

export {styles};
