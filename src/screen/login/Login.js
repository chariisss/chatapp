import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  Pressable,
  ScrollView,
  TextInput,
  Alert,
  Button,
} from 'react-native';
import Colors, {
  orange,
  orange2,
  gray,
  dark,
  dark2,
  white,
  white2,
  green,
  dark3,
} from '../../utils/Colors';
import ButtonC from '../../component/ButtonC';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {heightPercentageToDP, widthPercentageToDP} from '../../utils/Query';
import {styles} from './style';
import {
  GoogleSignin,
  GoogleSigninButton,
} from '@react-native-google-signin/google-signin';
GoogleSignin.configure({
  scopes: ['https://www.googleapis.com/auth/userinfo.profile'],
  webClientId:
    '207854622990-dkij079hqnqls8toomii0r5jchgitpfb.apps.googleusercontent.com',
  offlineAccess: true,
});

export class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      loading: false,
      userGoogleInfo: {},
      loaded: false,
    };
  }

  signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      this.setState({
        userGoogleInfo: userInfo,
        loaded: true,
      });
      console.log(this.state.userGoogleInfo);
      // AsyncStorage.setItem('user', JSON.stringify(userInfo))
    } catch (error) {
      console.log(error.message);
    }
  };

  submit = async () => {
    const {email, password} = this.state;
    if (email == 'charismakurniawan@gmail.com' && password == '1qazxsw2') {
      const dataUser = {pass: password, username: email};
      const jsonUser = JSON.stringify(dataUser);
      await AsyncStorage.setItem('data', jsonUser);

      this.props.navigation.navigate('Home');
    } else {
      alert('Email dan Password yang anda masukan salah');
    }
  };

  render() {
    return (
      <ScrollView style={styles.global}>
        <View style={{alignItems: 'center', justifyContent: 'center'}}>
          <Text style={styles.textJudul}>Chat App</Text>
        </View>
        <View style={styles.container}>
          <Text style={styles.textLogin}>Log In</Text>
          <Text style={styles.textSubLogin}>Login into your account</Text>
          <View style={[styles.button, {backgroundColor: dark2}]}>
            <Ionicons name="mail" size={25} color={white} />
            <TextInput
              placeholder="Email"
              placeholderTextColor={white2}
              style={styles.textInput}
              onChangeText={e => {
                this.setState({email: e});
              }}
            />
          </View>
          <View
            style={[
              styles.button,
              {marginTop: heightPercentageToDP(2), backgroundColor: dark3},
            ]}>
            <Ionicons name="lock-closed" size={25} color={gray} />
            <TextInput
              placeholder="Password"
              placeholderTextColor={gray}
              secureTextEntry={true}
              style={styles.textInput}
              onChangeText={e => {
                this.setState({password: e});
              }}
            />
          </View>
          <ButtonC
            text="Login"
            onPress={this.submit}
            // onPress={() => {
            //   this.props.navigation.navigate('Home');
            // }}
          />
          <GoogleSigninButton
            onPress={this.signIn}
            size={GoogleSigninButton.Size.Wide}
            color={GoogleSigninButton.Color.Dark}
            style={{
              width: widthPercentageToDP(82),
              height: heightPercentageToDP(7.2),
              marginTop: heightPercentageToDP(2),
            }}
          />
          {this.state.loaded ? (
            <View>
              <Text>{this.state.userGoogleInfo.user.name}</Text>
              <Text>{this.state.userGoogleInfo.user.mail}</Text>
              <Image
                style={{
                  width: widthPercentageToDP(80),
                  height: heightPercentageToDP(7),
                }}
                source={{uri: this.state.userGoogleInfo.user.photo}}
              />
            </View>
          ) : (
            <Text style={{color: white}}></Text>
          )}
          <Text style={styles.forgotPassword}>
            Forgot Password?{' '}
            <Text
              style={{color: orange}}
              onPress={() => {
                this.props.navigation.navigate('Reset Password');
              }}>
              Reset Password
            </Text>
          </Text>
          <Text style={{color: white2}}>
            Don't have an account?{' '}
            <Text
              style={{color: orange}}
              onPress={() => {
                this.props.navigation.navigate('Sign Up');
              }}>
              Sign Up
            </Text>
          </Text>
        </View>
      </ScrollView>
    );
  }
}

export default Login;
