import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  Pressable,
  ScrollView,
  TextInput,
} from 'react-native';
import Colors, {
  orange,
  orange2,
  gray,
  gray2,
  dark,
  dark2,
  dark3,
  dark4,
  white,
  white2,
  green,
} from '../../utils/Colors';
import ButtonC from '../../component/ButtonC';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {styles} from './style';
import {heightPercentageToDP, widthPercentageToDP} from '../../utils/Query';

export class NewPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      viewPassword: true,
    };
  }
  render() {
    return (
      <ScrollView style={styles.global}>
        <Ionicons
          name="chevron-back-circle"
          size={33}
          style={styles.backIcon}
          onPress={() => {
            this.props.navigation.navigate('Login');
          }}
        />
        <View style={{alignItems: 'center', justifyContent: 'center'}}>
          <Text style={styles.textJudul}>Chat App</Text>
        </View>
        <View style={styles.container}>
          <Text style={styles.textSignUp}>New Password</Text>
          <Text style={styles.textSubSignUp}>Enter your new password</Text>
          <Text style={styles.miniText}>Enter default password</Text>
          <View
            style={[
              styles.button,
              {
                backgroundColor: dark4,
                marginBottom: heightPercentageToDP(2),
              },
            ]}>
            <Ionicons name="lock-closed" size={25} color={gray} />
            <TextInput
              placeholder="Default Password"
              placeholderTextColor={gray}
              style={styles.textInput}
            />
          </View>
          <Text style={styles.miniText}>Enter new password</Text>
          <View
            style={[
              styles.button,
              {
                backgroundColor: dark4,
              },
            ]}>
            <Ionicons name="lock-closed" size={25} color={gray} />
            <TextInput
              placeholder="New Password"
              placeholderTextColor={gray}
              style={styles.textInput}
              secureTextEntry={this.state.viewPassword}
            />
            <Ionicons
              name={this.state.viewPassword ? 'eye-off-outline' : 'eye-outline'}
              size={25}
              color={gray}
              style={{marginLeft: 'auto', marginRight: widthPercentageToDP(5)}}
              onPress={() => {
                this.setState({viewPassword: !this.state.viewPassword});
              }}
            />
          </View>
          <ButtonC
            text="Reset Password"
            onPress={() => {
              this.props.navigation.navigate('Home');
            }}
          />
        </View>
      </ScrollView>
    );
  }
}

export default NewPassword;
