import {StyleSheet} from 'react-native';
import Colors, {
  orange,
  orange2,
  gray,
  dark,
  dark2,
  dark3,
  dark4,
  white,
  white2,
  green,
} from '../../utils/Colors';
import {heightPercentageToDP, widthPercentageToDP} from '../../utils/Query';

const styles = StyleSheet.create({
  global: {
    flex: 1,
    backgroundColor: dark,
  },
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  backIcon: {
    marginBottom: heightPercentageToDP(3),
    marginTop: heightPercentageToDP(2),
    marginLeft: widthPercentageToDP(5),
    color: white,
  },
  textJudul: {
    fontSize: heightPercentageToDP(7),
    color: orange,
    fontWeight: 'bold',
  },
  textSignUp: {
    fontSize: heightPercentageToDP(4),
    color: orange,
    fontWeight: 'bold',
    marginTop: heightPercentageToDP(6),
  },
  textSubSignUp: {
    fontSize: heightPercentageToDP(2.2),
    color: orange,
    marginTop: heightPercentageToDP(1),
    marginBottom: heightPercentageToDP(8),
  },
  button: {
    height: heightPercentageToDP(7),
    width: widthPercentageToDP(90),
    borderColor: orange,
    borderWidth: 1,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: widthPercentageToDP(5),
  },
  textInput: {
    color: white2,
    marginLeft: widthPercentageToDP(3),
    width: '100%',
  },
  miniText: {
    marginRight: 'auto',
    marginLeft: widthPercentageToDP(5),
    marginBottom: heightPercentageToDP(1),
    color: gray,
    fontSize: 12,
  },
});

export {styles};
