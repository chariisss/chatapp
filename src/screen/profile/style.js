import {StyleSheet} from 'react-native';
import Colors, {
  orange,
  orange2,
  gray,
  gray2,
  gray3,
  dark,
  dark2,
  dark3,
  dark4,
  white,
  white2,
  green,
  chocolate,
} from '../../utils/Colors';
import {heightPercentageToDP, widthPercentageToDP} from '../../utils/Query';

const styles = StyleSheet.create({
  global: {
    flex: 1,
    backgroundColor: dark,
  },
  topBar: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: widthPercentageToDP(5),
    marginHorizontal: widthPercentageToDP(5),
  },
  iconBack: {
    color: white,
    position: 'absolute',
    left: 0,
  },
  textInfo: {
    color: orange,
    fontSize: heightPercentageToDP(3.3),
    fontWeight: 'bold',
  },
  tabFoto: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: heightPercentageToDP(5),
  },
  profile: {
    width: widthPercentageToDP(30),
    height: widthPercentageToDP(30),
    backgroundColor: chocolate,
    borderRadius: 50,
    overflow: 'hidden',
    borderWidth: 7,
    borderColor: chocolate,
  },
  textName: {
    fontSize: widthPercentageToDP(6),
    marginTop: heightPercentageToDP(2),
    color: orange,
    fontWeight: 'bold',
  },
  textStatus: {
    fontSize: widthPercentageToDP(5),
    color: gray3,
  },
  list: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: heightPercentageToDP(8),
    width: widthPercentageToDP(100),
    paddingHorizontal: widthPercentageToDP(5),
  },
  textList: {color: white2, fontSize: widthPercentageToDP(4.5)},
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 'auto',
    marginBottom: heightPercentageToDP(5),
  },
});

export {styles};
