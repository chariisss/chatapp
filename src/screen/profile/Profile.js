import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  Pressable,
  ScrollView,
  TextInput,
} from 'react-native';
import Colors, {
  orange,
  orange2,
  gray,
  gray2,
  gray3,
  dark,
  dark2,
  white,
  white2,
  green,
  dark3,
  chocolate,
} from '../../utils/Colors';
import ButtonC from '../../component/ButtonC';
import ChatItemC from '../../component/ChatItemC';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {heightPercentageToDP, widthPercentageToDP} from '../../utils/Query';
import {styles} from './style';
import AsyncStorage from '@react-native-async-storage/async-storage';

export class Profile extends Component {
  logout = () => {
    AsyncStorage.clear();
    this.props.navigation.navigate('Login');
  };
  render() {
    return (
      <View style={styles.global}>
        <View style={styles.topBar}>
          <Ionicons
            name="chevron-back-circle"
            size={33}
            style={styles.iconBack}
            onPress={() => {
              this.props.navigation.goBack('');
            }}
          />
          <Text style={styles.textInfo}>Info</Text>
        </View>
        <View style={styles.tabFoto}>
          <TouchableOpacity style={styles.profile}>
            <Image
              source={require('../../img/pp.jpeg')}
              style={{width: '100%', height: '100%'}}
            />
          </TouchableOpacity>
          <Text style={styles.textName}>Charisma Kurniawan</Text>
          <Text style={styles.textStatus}>All is well</Text>
        </View>
        <View style={styles.list}>
          <Text style={styles.textList}>Phone</Text>
          <Text style={styles.textList}>+62 822 1832 7910</Text>
        </View>
        <View style={[styles.list, {backgroundColor: dark2}]}>
          <Text style={styles.textList}>Account</Text>
          <TouchableOpacity>
            <Ionicons name="chevron-forward" size={30} color={white2} />
          </TouchableOpacity>
        </View>
        <View style={styles.list}>
          <Text style={styles.textList}>Notification</Text>
          <TouchableOpacity>
            <Ionicons name="chevron-forward" size={30} color={white2} />
          </TouchableOpacity>
        </View>
        <View style={[styles.list, {backgroundColor: dark2}]}>
          <Text style={styles.textList}>Invite Your Friend</Text>
          <TouchableOpacity>
            <Ionicons name="chevron-forward" size={30} color={white2} />
          </TouchableOpacity>
        </View>
        <View style={styles.button}>
          <ButtonC
            text="Edit"
            onPress={() => {
              this.props.navigation.navigate('Home');
            }}
          />
          <ButtonC text="Logout" onPress={this.logout} />
        </View>
      </View>
    );
  }
}

export default Profile;
