import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {dark, orange, white} from '../../utils/Colors';
import {heightPercentageToDP, widthPercentageToDP} from '../../utils/Query';
import AsyncStorage from '@react-native-async-storage/async-storage';

export class Splash extends Component {
  async componentDidMount() {
    const data = await this.getData();
    console.log(data);
    setTimeout(() => {
      if (data != null) {
        this.props.navigation.replace('Home');
      } else {
        this.props.navigation.replace('Login');
      }
    }, 3000);
  }

  getData = async () => {
    try {
      const valueData = await AsyncStorage.getItem('data');
      return valueData != null ? JSON.parse(valueData) : null;
    } catch (error) {}
  };

  render() {
    return (
      <View style={{flex: 1, backgroundColor: orange}}>
        <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
          <Text
            style={{
              color: white,
              fontSize: widthPercentageToDP(10),
              fontWeight: 'bold',
            }}>
            Messenger
          </Text>
          <Text
            style={{
              color: white,
              fontSize: widthPercentageToDP(15),
              fontWeight: 'bold',
              marginTop: heightPercentageToDP(5),
            }}>
            APP UI
          </Text>
        </View>
      </View>
    );
  }
}

export default Splash;
