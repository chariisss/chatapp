import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  Pressable,
  ScrollView,
  TextInput,
} from 'react-native';
import Colors, {
  orange,
  orange2,
  gray,
  dark,
  dark2,
  white,
  white2,
  green,
  dark3,
} from '../../utils/Colors';
import ButtonC from '../../component/ButtonC';
import ChatItemC from '../../component/ChatItemC';
import Ionicons from 'react-native-vector-icons/Ionicons';
import firestore, {firebase} from '@react-native-firebase/firestore';
import {heightPercentageToDP, widthPercentageToDP} from '../../utils/Query';
import {styles} from './style';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import PushNotification from 'react-native-push-notification';
import Firebase from '@react-native-firebase/app';
import AsyncStorage from '@react-native-async-storage/async-storage';

export class Home extends Component {
  constructor() {
    super();
    this.state = {
      data: {
        nama: 'default name',
        uid: 'default uid',
        foto: 'default foto',
        message: 'default message',
      },
      chat: [],
      profilePicture: [],
      inbox: [
        {
          title: '',
          body: '',
        },
      ],
    };
  }

  async componentDidMount() {
    // Firebase.initializeApp(this);

    // PushNotification.configure({
    //   // (optional) Called when Token is generated (iOS and Android)
    //   onRegister: function (token) {
    //     console.log('TOKEN:', token);
    //   },

    //   // (required) Called when a remote is received or opened, or local notification is opened
    //   onNotification: function (notification) {
    //     console.log('NOTIFICATION:', notification);

    //     // process the notification

    //     // (required) Called when a remote is received or opened, or local notification is opened
    //     notification.finish(PushNotificationIOS.FetchResult.NoData);
    //   },

    //   // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
    //   onAction: function (notification) {
    //     console.log('ACTION:', notification.action);
    //     console.log('NOTIFICATION:', notification);

    //     // process the action
    //   },

    //   // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
    //   onRegistrationError: function (err) {
    //     console.error(err.message, err);
    //   },

    //   // IOS ONLY (optional): default: all - Permissions to register.
    //   permissions: {
    //     alert: true,
    //     badge: true,
    //     sound: true,
    //   },

    //   // Should the initial notification be popped automatically
    //   // default: true
    //   popInitialNotification: true,

    //   /**
    //    * (optional) default: true
    //    * - Specified if permissions (ios) and token (android and ios) will requested or not,
    //    * - if not, you must call PushNotificationsHandler.requestPermissions() later
    //    * - if you are not using remote notification or do not have Firebase installed, use this:
    //    *     requestPermissions: Platform.OS === 'ios'
    //    */
    //   requestPermissions: true,
    // });

    // firestore()
    //   .collection('contact')
    //   .get()
    //   .then(value => {
    //     let penampung = value.docs.map(result => {
    //       return result.data();
    //     });
    //     this.setState({
    //       chat: penampung,
    //     });
    //   });

    firestore()
      .collection('profile')
      .get()
      .then(value => {
        let penampung2 = value.docs.map(result => {
          return result.data();
        });
        this.setState({
          profilePicture: penampung2,
        });
      });
    // const lastData = await AsyncStorage.getItem('inbox');
    // console.log(`==================`, lastData);
    // const data = await this.getData();
    // this.setState({
    //   inbox: [...this.state.inbox, data],
    // });

    AsyncStorage.getItem('inbox')
      .then(valueMessage => {
        const inbox = JSON.parse(valueMessage);
        console.log(valueMessage);
        if (valueMessage) {
          this.setState({
            inbox: inbox,
          });
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  // getData = async () => {
  //   try {
  //     const valueInbox = await AsyncStorage.getItem('inbox');
  //     return valueInbox != null ? JSON.parse(valueInbox) : null;
  //   } catch (error) {}
  // };

  // componentDidMount() {
  //   const chat = [];
  //   firestore()
  //     .collection('contact')
  //     .get()
  //     .then(querySnapshot => {
  //       querySnapshot.forEach(documentSnapshot => {
  //         chat.push(documentSnapshot.data());
  //       });
  //     })
  //     .then(() => {
  //       this.setState({
  //         list: chat,
  //       });
  //     });

  //   const profile = [];
  //   firestore()
  //     .collection('profile')
  //     .get()
  //     .then(querySnapshot => {
  //       querySnapshot.forEach(documentSnapshot => {
  //         profile.push(documentSnapshot.data());
  //       });
  //     })
  //     .then(() => {
  //       this.setState({
  //         profilePicture: profile,
  //       });
  //     });
  // }

  // real time
  // componentDidMount() {
  //   const penampung2 = [];
  //   firestore()
  //     .collection('profilePicture')
  //     .get()
  //     .then(querySnapshot => {
  //       querySnapshot.forEach(documentSnapshot => {
  //         penampung2.push(documentSnapshot.data());
  //       });
  //     })
  //     .then(() => {
  //       this.setState({
  //         profilePicture: penampung2,
  //       });
  //     });
  // }

  render() {
    return (
      <View style={styles.global}>
        <View style={styles.container}>
          <View style={styles.topBar}>
            <Text style={{fontSize: 22, color: orange, fontWeight: 'bold'}}>
              Chat App
            </Text>
            <TouchableOpacity
              style={styles.profile}
              onPress={() => {
                this.props.navigation.navigate('Profile');
              }}>
              {this.state.profilePicture.map((e, index) => (
                <Image
                  source={{uri: e.pp}}
                  style={{
                    width: '100%',
                    height: '100%',
                  }}
                  key={index}
                />
              ))}
            </TouchableOpacity>
          </View>
          <View style={styles.searchButton}>
            <TextInput
              placeholder="Search for message ..."
              style={{width: '100%'}}
            />
          </View>
        </View>
        <TouchableOpacity style={styles.button}>
          <Ionicons name="add-outline" size={35} color={white} />
        </TouchableOpacity>
        <ScrollView>
          {this.state.inbox.map((value, index) => (
            <ChatItemC name={value.title} text={value.body} />
          ))}
        </ScrollView>
      </View>
    );
  }
}

export default Home;
