import {StyleSheet} from 'react-native';
import Colors, {
  orange,
  orange2,
  gray,
  dark,
  dark2,
  dark3,
  dark4,
  white,
  white2,
  green,
  chocolate,
} from '../../utils/Colors';
import {heightPercentageToDP, widthPercentageToDP} from '../../utils/Query';

const styles = StyleSheet.create({
  global: {
    flex: 1,
    backgroundColor: dark,
  },
  container: {
    marginHorizontal: widthPercentageToDP(5),
    marginTop: heightPercentageToDP(1),
  },
  topBar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  profile: {
    width: widthPercentageToDP(12),
    height: widthPercentageToDP(12),
    backgroundColor: white,
    borderRadius: 50,
    overflow: 'hidden',
    borderWidth: 3,
    borderColor: chocolate,
  },
  searchButton: {
    height: heightPercentageToDP(7),
    width: widthPercentageToDP(90),
    backgroundColor: white2,
    paddingLeft: widthPercentageToDP(5),
    marginTop: heightPercentageToDP(1),
    borderRadius: 20,
    marginBottom: heightPercentageToDP(2),
  },
  button: {
    height: heightPercentageToDP(8),
    width: heightPercentageToDP(8),
    backgroundColor: orange,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
    position: 'absolute',
    marginTop: heightPercentageToDP(87),
    marginLeft: widthPercentageToDP(80),
    zIndex: 1,
  },
});

export {styles};
