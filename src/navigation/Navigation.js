import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import Onboarding from '../screen/onboarding/Onboarding';
import Login from '../screen/login/Login';
import SignUp from '../screen/signup/SignUp';
import ResetPassword from '../screen/reset/ResetPassword';
import CodeVerif from '../screen/codeSend/CodeVerif';
import NewPassword from '../screen/newPassword/NewPassword';
import Home from '../screen/home/Home';
import Profile from '../screen/profile/Profile';
import Chat from '../screen/chat/Chat';
import Splash from '../screen/splash/Splash';
const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

export class Navigation extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Splash"
            component={Splash}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="Onboarding"
            component={Onboarding}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="Login"
            component={Login}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="Sign Up"
            component={SignUp}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="Reset Password"
            component={ResetPassword}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="Code Verif"
            component={CodeVerif}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="New Password"
            component={NewPassword}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="Home"
            component={Home}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="Profile"
            component={Profile}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="Chat"
            component={Chat}
            options={{headerShown: false}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

export default Navigation;
