import React, {Component} from 'react';
import {Pressable, Text, View} from 'react-native';
import Colors, {
  orange,
  orange2,
  gray,
  dark,
  dark2,
  white,
  white2,
  green,
  dark3,
} from '../utils/Colors';
import ButtonComponent from './ButtonC';
import {heightPercentageToDP, widthPercentageToDP} from '../utils/Query';

export class ButtonC extends Component {
  render() {
    return (
      <Pressable
        style={{
          height: heightPercentageToDP(7),
          width: widthPercentageToDP(80),
          backgroundColor: orange2,
          borderRadius: 10,
          marginTop: heightPercentageToDP(4),
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={this.props.onPress}>
        <Text style={{color: white, fontSize: widthPercentageToDP(5)}}>
          {this.props.text}
        </Text>
      </Pressable>
    );
  }
}

export default ButtonC;
