import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  Pressable,
  ScrollView,
  TextInput,
  useWindowDimensions,
  StyleSheet,
} from 'react-native';
import Colors, {
  orange,
  orange2,
  gray,
  dark,
  dark2,
  white,
  white2,
  green,
  dark3,
} from '../utils/Colors';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {heightPercentageToDP, widthPercentageToDP} from '../utils/Query';

export default class ChatItemC extends Component {
  render() {
    return (
      <TouchableOpacity
        style={{
          backgroundColor: this.props.bgcolor,
        }}
        onPress={() => {
          this.props.navigation.navigate('Chat');
        }}>
        <View style={styles.container}>
          <View style={{flexDirection: 'row'}}>
            <View style={styles.profil}>
              <Image
                source={{uri: this.props.img}}
                style={{width: '100%', height: '100%'}}
              />
            </View>
            <View style={{marginLeft: widthPercentageToDP(3)}}>
              <Text style={styles.name}>{this.props.name}</Text>
              <Text style={styles.message} numberOfLines={1}>
                {this.props.text}
              </Text>
            </View>
            <View
              style={{
                marginLeft: 'auto',
                justifyContent: 'space-around',
                alignItems: 'flex-end',
              }}>
              <Text
                style={{
                  fontSize: heightPercentageToDP(2),
                  color: white2,
                }}>
                17.00
              </Text>
              <View
                style={[
                  styles.totalChat,
                  {
                    backgroundColor:
                      this.props.totalText > 0 ? orange : this.props.bgcolor,
                  },
                ]}>
                <Text style={{color: dark3, fontSize: widthPercentageToDP(3)}}>
                  {this.props.totalText}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: heightPercentageToDP(10),
    width: widthPercentageToDP(100),
    paddingHorizontal: widthPercentageToDP(5),
    justifyContent: 'center',
  },
  profil: {
    height: heightPercentageToDP(8),
    width: heightPercentageToDP(8),
    backgroundColor: gray,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: orange,
    overflow: 'hidden',
  },
  name: {
    fontSize: heightPercentageToDP(2.5),
    color: white2,
    fontWeight: 'bold',
  },
  message: {
    fontSize: heightPercentageToDP(2),
    color: white2,
    paddingRight: widthPercentageToDP(20),
  },
  totalChat: {
    width: widthPercentageToDP(6),
    height: widthPercentageToDP(6),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
});
